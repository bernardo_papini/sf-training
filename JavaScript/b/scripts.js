const fullDate = new Date().toLocaleTimeString('en-us', { weekday: 'long' });
const dayOfTheWeek = fullDate.split(' ')[0]
const hour = new Date().toLocaleTimeString('en-us', { hour: 'numeric', hour12: true });
const minutes = new Date().getMinutes()
const seconds = new Date().getSeconds()

console.log(`Today is ${dayOfTheWeek} and Current time is ${hour} : ${minutes} : ${seconds}`);
