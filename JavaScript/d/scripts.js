const number = 1994;

const reverseNumber = (number) => parseInt(String(number).split("").reverse().join(""), 10);

console.log(reverseNumber(number));