let wordCombinations = (word) => {
  let wordLength = word.length;
  let result = [];
  let index = 0;

  while (index < wordLength) {
    let character = word.charAt(index);
    let x;
    let array = [character];

    for (x in result) {
      array.push("" + result[x] + character);
    }

    result = result.concat(array);
    index++;
  }
  return result;
};

console.log(wordCombinations("Havaianas"));
