const charCount = (string, letter) => {
  let letterCount = 0;
  for (let position = 0; position < string.length; position++) {
    if (string.charAt(position) == letter) {
      letterCount += 1;
    }
  }
  return letterCount;
};

console.log(charCount("Havaianas", "a"));
