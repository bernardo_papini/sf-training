function check() {
  var number;
  number = Number(document.querySelector(".number").value);
  if (number === 0) {
		alert('Please enter a valid number')
	} else if (number % 2 == 0) {
    alert(`${number} is even`);
  } else {
    alert(`${number} is odd`);
  }
}
