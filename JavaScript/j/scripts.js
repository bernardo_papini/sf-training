const array1 = ['car', 'trees', 'sun', 2, 4, 'street']
const array2 = ['street', 'flowers', 3, 2, 4, 'bike', 'trees']
const newArray = [...array1, ...array2]

const filteredNewArray = newArray.filter((item, index, self) => {
	return index === self.indexOf(item);
})

console.log(filteredNewArray);