const isValidUrl = (string) => {
  const match = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/gm;
  return match.test(string);
}

let string1 = "https://havaianas.com"

isValidUrl(string1) ? console.log("URL is valid") : console.log("URL is invalid")