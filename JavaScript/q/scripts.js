const profile = {
  name: "Bernardo",
  age: 28,
  country: "Brazil",
};

console.log(
  `My name is ${profile.name}, I'm ${profile.age} and my coutry is ${profile.country}`
);

let profileValues = "";
for (let x in profile) {
  profileValues += profile[x] + " ";
}

console.log(profileValues);

const result = JSON.stringify(profile);

console.log(result);