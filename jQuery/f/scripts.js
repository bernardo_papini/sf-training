$(".clickme").mousedown((event) => {
  switch (event.which) {
    case 1:
      alert("Left mouse button was pressed");
      break;
    case 2:
      alert("Middle mouse button was pressed");
      break;
    case 3:
      alert("Right mouse button pressed");
      break;
    default:
      alert("A different button was pressed");
  }
});
