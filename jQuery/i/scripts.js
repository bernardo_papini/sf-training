$("#form").submit(function (e) {
  const email = $("#email").val();
  const pattern = new RegExp(
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
  );
  if (email === null || email === undefined || email === "") {
    alert("Please enter an email");
  } else if (pattern.test(email)) {
    alert(`The e-mail '${email}' is a valid e-mail`);
  } else {
    alert(`The e-mail '${email}' is a invalid e-mail`);
  }
  e.preventDefault();
});
