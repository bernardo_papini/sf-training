$(function () {
  // dialog 1
  $("#dialog1").dialog({
    autoOpen: false,
    show: {
      duration: 350,
    },
    hide: {
      duration: 350,
    },
  });
  $("#open1").click(function () {
    $("#dialog1").dialog("open");
		$("#dialog2").dialog("close");
		$("#dialog3").dialog("close");
  });

  //dialog 2
  $("#dialog2").dialog({
    autoOpen: false,
    show: {
      duration: 350,
    },
    hide: {
      duration: 350,
    },
  });
  $("#open2").click(function () {
    $("#dialog2").dialog("open");
		$("#dialog1").dialog("close");
		$("#dialog3").dialog("close");
  });

  // dialog 3
  $("#dialog3").dialog({
    autoOpen: false,
    show: {
      duration: 350,
    },
    hide: {
      duration: 350,
    },
  });
  $("#open3").click(function () {
    $("#dialog3").dialog("open");
		$("#dialog2").dialog("close");
		$("#dialog1").dialog("close");
  });
});
