$(document).ready(function () {
  var result = document.getElementById("done");
	
  $(":input")
    .on("focus", function () {
      result.innerHTML += `Focused Element: ${this.tagName} | Id: ${this.id}`;
    })
    .on("blur", function () {
			result.innerHTML = '';
    });
});
