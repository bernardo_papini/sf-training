$(document).ready(function () {
	const result = $("#result").get(0)
	const squareTagName = $("#square").prop("tagName")

  $("#square")
    .on("focus", function () {
			$("#square").css("background-color", "red")
		})
    .on("blur", function () {
			$("#square").css("background-color", "")
    })
		.on("click", function () {
			result.innerHTML += `Tag name: ${squareTagName}`
		})
});
